package com.agiletestingalliance;
import static org.junit.Assert.*;
import org.junit.Test;


public class MinMaxTest {

   @Test
    public void testReturnMax1() throws Exception {

        final int tempVar= new MinMax().returnMax(10,1);
        assertEquals("returnMax1", 10  , tempVar);

    }

   @Test
    public void testReturnMax2() throws Exception {

        final int tempVar= new MinMax().returnMax(1,10);
        assertEquals("returnMax1", 10  , tempVar);

    }

  }

